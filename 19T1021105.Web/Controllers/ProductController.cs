﻿using _19T1021105.BusinessLayers;
using _19T1021105.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _19T1021105.Web.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    [RoutePrefix("product")]
    public class ProductController : Controller
    {
        private const int PAGE_SIZE = 5;
        private const string PRODUCT_SEARCH = "ProductCondition";
        /// <summary>
        /// Tìm kiếm, hiển thị mặt hàng dưới dạng phân trang
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(int CategoryID = 0, int SuplierID = 0)
        {
            Models.PaginationSearchInput condition = Session[PRODUCT_SEARCH] as Models.PaginationSearchInput; //ép kiểu
            if (condition == null)
            {
                condition = new Models.PaginationSearchInput()
                {
                    Page = 1,
                    PageSize = PAGE_SIZE,
                    SearchValue = "",
                    CategoryID = 0,
                    SuplierID = 0
                };
            }

            //if(CategoryID > 0 || SuplierID > 0)
            //{
            //    condition.CategoryID = CategoryID;
            //    condition.SuplierID = SuplierID;
            //    condition.SearchValue = "";
            //}

            return View(condition);
        }

        public ActionResult Search(Models.PaginationSearchInput condition)
        {
            int rowCount = 0;
            var data = ProductDataService.ListOfProducts(condition.Page, condition.PageSize, condition.SearchValue, condition.CategoryID, condition.SuplierID, out rowCount);
            Models.ProductSearchOutput result = new Models.ProductSearchOutput
            {
                Page = condition.Page,
                PageSize = condition.PageSize,
                SearchValue = condition.SearchValue,
                RowCount = rowCount,
                Data = data,
                CategoryID = condition.CategoryID,
                SuplierID = condition.SuplierID
            };

            //lưu condition vào session
            Session[PRODUCT_SEARCH] = condition;

            return View(result);
        }
        /// <summary>
        /// Tạo mặt hàng mới
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            var data = new Product()
            {
                ProductID = 0
            };
            ViewBag.Title = "Bổ sung mặt hàng";
            return View(data);
        }
        /// <summary>
        /// Cập nhật thông tin mặt hàng, 
        /// Hiển thị danh sách ảnh và thuộc tính của mặt hàng, điều hướng đến các chức năng
        /// quản lý ảnh và thuộc tính của mặt hàng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>        
        public ActionResult Edit(int id = 0)
        {
            ViewBag.Title = "Cập nhật thông tin mặt hàng";
            if (id <= 0)
                return RedirectToAction("Index");

            var data = ProductDataService.GetProduct(id);

            if (data == null)
                return RedirectToAction("Index");

            return View(data);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Save(Product data, HttpPostedFileBase uploadPhoto)
        {
            if (uploadPhoto != null)
            {
                string path = Server.MapPath("~/Images/Products");
                string fileName = $"{DateTime.Now.Ticks}_{uploadPhoto.FileName}";
                string filePath = System.IO.Path.Combine(path, fileName);
                uploadPhoto.SaveAs(filePath);

                data.Photo = fileName;

            }

            if (string.IsNullOrWhiteSpace(data.ProductName))
                ModelState.AddModelError(nameof(data.ProductName), "*");

            if (string.IsNullOrWhiteSpace(data.Unit))
                ModelState.AddModelError(nameof(data.Unit), "*");

            if (string.IsNullOrEmpty(data.SuplierID.ToString()))
                ModelState.AddModelError(nameof(data.SuplierID), "*");

            if (string.IsNullOrEmpty(data.CategoryID.ToString()))
                ModelState.AddModelError(nameof(data.CategoryID), "*");

            if (string.IsNullOrWhiteSpace(data.Price.ToString()))
                ModelState.AddModelError(nameof(data.Price), "*");

            if (data.Price <= 0)
                ModelState.AddModelError(nameof(data.Price), "*");

            if (string.IsNullOrWhiteSpace(data.Photo))
                ModelState.AddModelError(nameof(data.Photo), "*");

            if (!ModelState.IsValid)
            {
                if (data.ProductID > 0)
                {
                    ViewBag.Title = "Cập nhật thông tin mặt hàng";
                    return View("Edit", data);
                }
                else
                {
                    ViewBag.Title = "Bổ sung mặt hàng";
                    return View("Create", data);
                }
            }

            if (data.ProductID == 0)
            {
                ProductDataService.AddProduct(data);
            }
            else
            {
                ProductDataService.UpdateProduct(data);
            }

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Xóa mặt hàng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>        
        public ActionResult Delete(int id = 0)
        {
            if (id <= 0)
                return RedirectToAction("Index");

            if (Request.HttpMethod == "POST")
            {
                ProductDataService.DeleteProduct(id);
                return RedirectToAction("Index");
            }

            var data = ProductDataService.GetProduct(id);

            if (data == null)
                return RedirectToAction("Index");

            return View(data);
        }

        /// <summary>
        /// Các chức năng quản lý ảnh của mặt hàng
        /// </summary>
        /// <param name="method"></param>
        /// <param name="productID"></param>
        /// <param name="photoID"></param>
        /// <returns></returns>
        [Route("photo/{method?}/{productID?}/{photoID?}")]
        public ActionResult Photo(string method = "add", int productID = 0, long photoID = 0)
        {
            var data = new ProductPhoto();
            switch (method)
            {
                case "add":
                    ViewBag.Title = "Bổ sung ảnh";
                    data = new ProductPhoto()
                    {
                        PhotoID = 0,
                        ProductID = productID
                    };
                    return View(data);
                case "edit":
                    ViewBag.Title = "Thay đổi ảnh";
                    if (photoID <= 0)
                        return RedirectToAction($"Edit/{productID}");

                    data = ProductDataService.GetPhoto(photoID);
                    if (data == null)
                        return RedirectToAction($"Edit/{productID}");

                    return View(data);
                case "delete":
                    ProductDataService.DeletePhoto(photoID);
                    return RedirectToAction($"Edit/{productID}"); //return RedirectToAction("Edit", new { productID = productID });
                default:
                    return RedirectToAction("Index");
            }
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SavePhoto(ProductPhoto data, HttpPostedFileBase uploadPhoto)
        {
            if (uploadPhoto != null)
            {
                string path = Server.MapPath("~/Images/Products");
                string fileName = $"{DateTime.Now.Ticks}_{uploadPhoto.FileName}";
                string filePath = System.IO.Path.Combine(path, fileName);
                uploadPhoto.SaveAs(filePath);

                data.Photo = $"Images/Products/{fileName}";

            }

            if (string.IsNullOrWhiteSpace(data.Description))
                ModelState.AddModelError(nameof(data.Description), "*");

            if (string.IsNullOrEmpty(data.DisplayOrder.ToString()))
                ModelState.AddModelError(nameof(data.DisplayOrder), "*");

            if (string.IsNullOrWhiteSpace(data.Photo))
                ModelState.AddModelError(nameof(data.Photo), "*");

            if (!ModelState.IsValid)
            {
                ViewBag.Title = data.PhotoID == 0 ? "Bổ sung ảnh" : "Cập nhật thông tin ảnh";
                return View("Photo", data);
            }

            if (data.PhotoID == 0)
            {
                ProductDataService.AddPhoto(data);
            }
            else
            {
                ProductDataService.UpdatePhoto(data);
            }

            return RedirectToAction("Edit/" + data.ProductID);
        }

        /// <summary>
        /// Các chức năng quản lý thuộc tính của mặt hàng
        /// </summary>
        /// <param name="method"></param>
        /// <param name="productID"></param>
        /// <param name="attributeID"></param>
        /// <returns></returns>
        [Route("attribute/{method?}/{productID}/{attributeID?}")]
        public ActionResult Attribute(string method = "add", int productID = 0, int attributeID = 0)
        {
            var data = new ProductAttribute();
            switch (method)
            {
                case "add":
                    ViewBag.Title = "Bổ sung thuộc tính";
                    data = new ProductAttribute()
                    {
                        AttributeID = 0,
                        ProductID = productID
                    };
                    return View(data);
                case "edit":
                    ViewBag.Title = "Thay đổi thuộc tính";
                    if (attributeID <= 0)
                        return RedirectToAction($"Edit/{productID}");

                    data = ProductDataService.GetAttribute(attributeID);

                    if (data == null)
                        return RedirectToAction($"Edit/{productID}");

                    return View(data);
                case "delete":
                    ProductDataService.DeleteAttribute(attributeID);
                    return RedirectToAction($"Edit/{productID}"); //return RedirectToAction("Edit", new { productID = productID });
                default:
                    return RedirectToAction("Index");
            }
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SaveAttribute(ProductAttribute data)
        {

            if (string.IsNullOrWhiteSpace(data.AttributeName))
                ModelState.AddModelError(nameof(data.AttributeName), "*");

            if (string.IsNullOrEmpty(data.AttributeValue))
                ModelState.AddModelError(nameof(data.AttributeValue), "*");

            if (!ModelState.IsValid)
            {
                ViewBag.Title = data.AttributeID == 0 ? "Bổ sung thuộc tính" : "Thay đổi thuộc tính";
                return View("Attribute", data);
            }

            if (data.AttributeID == 0)
            {
                ProductDataService.AddAttribute(data);
            }
            else
            {
                ProductDataService.UpdateAttribute(data);
            }

            return RedirectToAction("Edit/" + data.ProductID);
        }
    }
}