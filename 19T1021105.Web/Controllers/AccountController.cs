﻿using _19T1021105.BusinessLayers;
using _19T1021105.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace _19T1021105.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Trang đăng nhập vào hệ thống
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(string userName = "", string password = "")
        {
            ViewBag.UserName = userName;
            if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(password))
            {
                ModelState.AddModelError("", "Nhập thông tin không đầy đủ");
                return View();
            }

            var userAccount = UserAccountService.Authorize(AccountTypes.Employee, userName, password);
            if (userAccount == null)
            {
                ModelState.AddModelError("", "Đăng nhập thất bại");
                return View();
            }

            //Ghi nhận cookie cho phiên đăng nhập
            string cookieString = Newtonsoft.Json.JsonConvert.SerializeObject(userAccount);
            FormsAuthentication.SetAuthCookie(cookieString, false);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Logout()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
        public ActionResult ChangePassword(PasswordResult data)
        {
            if (string.IsNullOrWhiteSpace(data.OldPassword))
                ModelState.AddModelError(nameof(data.OldPassword), "Vui lòng nhập mật khẩu cũ");

            if (UserAccountService.Authorize(AccountTypes.Employee, Converter.CookieToUserAccount(User.Identity.Name).Email, data.OldPassword ?? "") == null)
                ModelState.AddModelError(nameof(data.OldPassword), "Mật khẩu không đúng");

            if (string.IsNullOrWhiteSpace(data.NewPassword))
                ModelState.AddModelError(nameof(data.NewPassword), "Vui lòng nhập mật khẩu mới");



            if (string.IsNullOrWhiteSpace(data.ReEnterPassword))
                ModelState.AddModelError(nameof(data.ReEnterPassword), "Vui lòng nhập lại mật khẩu mới");

            if (data.NewPassword != data.ReEnterPassword && !string.IsNullOrEmpty(data.NewPassword) && !string.IsNullOrEmpty(data.ReEnterPassword))
                ModelState.AddModelError(nameof(data.ReEnterPassword), "Vui lòng nhập mật khẩu mới trùng khớp");


            if (!ModelState.IsValid)
            {
                return View(data);
            }

            var changePasswordUser = UserAccountService.ChangePassword(AccountTypes.Employee, Converter.CookieToUserAccount(User.Identity.Name).Email, data.OldPassword, data.NewPassword);

            if (!changePasswordUser)
            {
                ModelState.AddModelError("", "Đổi mật khẩu thất bại");
                return View();
            }

            Session.Clear();
            return RedirectToAction("Index", "Home");
        }


    }
}