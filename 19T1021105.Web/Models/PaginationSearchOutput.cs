﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _19T1021105.Web.Models
{
    /// <summary>
    /// Lớp cơ sở dùng để biểu diễn kết quả tìm kiếm dưới dạng phân trang
    /// </summary>
    public abstract class PaginationSearchOutput
    {
        /// <summary>
        /// Trang được hiển thị
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// Số dòng trên mỗi trang
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Giá trị tìm kiếm
        /// </summary>
        public string SearchValue { get; set; }
        /// <summary>
        /// Số dòng
        /// </summary>
        public int RowCount { get; set; }
        /// <summary>
        /// Số trang 
        /// </summary>
        public int PageCount 
        {
            get
            {
                if (PageSize == 0)
                    return 1;
                int p = RowCount / PageSize;
                if (RowCount % PageSize > 0)
                    p += 1;
                return p;
            }
        }
        public int CategoryID { get; set; }
        public int SuplierID { get; set; }
        public int Status { get; set; }
    }
}