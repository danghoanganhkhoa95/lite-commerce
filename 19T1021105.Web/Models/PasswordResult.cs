﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _19T1021105.Web.Models
{
    public class PasswordResult
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string OldPassword { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string NewPassword { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ReEnterPassword { get; set; }
    }
}